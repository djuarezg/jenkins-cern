Overview
========

### Goal

This is the main repository and internal documentation
for the [Continuous Integration with Jenkins](http://information-technology.web.cern.ch/services/CI-Jenkins)
service.

See also the Service-Now [Service Element](https://cern.service-now.com/service-portal/service-element.do?name=CI-Jenkins).

### How it is done

This project customizes https://github.com/openshift/jenkins for CERN needs, using
the layering approach (cf. README of that project).
It aims at providing an instance of Jenkins that can run in Openshift.

It is meant to be deployed as an [externally managed resource](https://espace.cern.ch/openshift-internal/_layouts/OneNote.aspx?id=%2Fopenshift-internal%2FShared%20Documents%2FOpenshift&wd=target%28Deployment.one%7CDACB26C0-65A0-4EEE-8B63-99CFEAEF4A66%2FExternally%20managed%20resources%7C231F33C7-8557-4F5B-A38D-0814216E12EB%2F%29)
in Openshift (as per procedures in
our Openshift docs). The docker image is built with Gitlab-CI and stored into the GitLab registry.
The image is then referenced from an ImageStream named `jenkins-cern` in the `openshift` namespace.

A template with all the necessary
components is also deployed, also in the `openshift` namespace.

Unlike the upstream image we use the LTS branch of Jenkins, so as to have something more stable (and minor releases
in case of critical bugs/security issues)

### Related resources

The jenkins slaves are built and stored in a different repository: https://gitlab.cern.ch/ci-tools/ci-worker

Jenkins documentation is maintained in https://gitlab.cern.ch/ci-tools/jenkinsdocs and
deployed to https://cern.ch/jenkinsdocs using service account `jenkinsdocs`.

E-group `jenkins-admins` is granted access to project resources (GitLab group https://gitlab.cern.ch/ci-tools)
and is by default granted SSO access to all Jenkins instances created from the Openshift template. This only
gives access to the Jenkins instance and does not include access to the containers or Openshift project
(this is reserved to Openshift admins).

Deployment
==========

Once the  _externally managed resource_
is created following [our Openshift docs](https://espace.cern.ch/openshift-internal/_layouts/OneNote.aspx?id=%2Fopenshift-internal%2FShared%20Documents%2FOpenshift&wd=target%28Deployment.one%7CDACB26C0-65A0-4EEE-8B63-99CFEAEF4A66%2FExternally%20managed%20resources%7C231F33C7-8557-4F5B-A38D-0814216E12EB%2F%29),
we have to create an ImageStream and Template in the `openshift` namespace

```bash
oc create -n openshift -f  - <<EOF
apiVersion: v1
kind: ImageStream
metadata:
  name: jenkins-cern
spec:
  dockerImageRepository: gitlab-registry.cern.ch/ci-tools/jenkins-cern
EOF
```

The initial deployment of the template is done by hand by running:

```bash
oc create -n openshift -f templates/jenkins-cern.yaml
```

For each Openshift cluster where we deploy the jenkins-cern _externally managed resource_, make sure to
obtain the corresponding serviceaccount token to set as secret variable in the GitLab-CI
configuration. This enables updating template, images and instances automatically from GitLab-CI.

By default, production Jenkins instances use the `stable` tag on the `jenkins-cern`
ImageStream. This tag is set in the ImageStream to point to an existing version tag
and indicate which version of the image to use in this deployment (there is no `stable` tag in the image
repository in the GitLab registry).

Apply the procedure to update the Jenkins master image below to build an image and set the `stable` tag.
To use an existing image, use `oc` manually:

```bash
# import all tags
oc import-image -n openshift jenkins-cern --all
# tag one version as stable
oc tag -n openshift jenkins-cern:2.32.1-1 jenkins-cern:stable
```

Updating Jenkins
================

Update template
---------------

To update the template, create a new branch and modify the template. GitLab-CI
will offer a manual trigger to deploy the new template to _dev_ cluster.

When satisfied, merge changes and apply a git tag in order to automatically
update the template in the _prod_ cluster. Use tags with Jenkins version and incremental
release number (e.g. `2.32.2-1`, `2.32.2-2` etc when the Jenkins version doesn't change).

The template gets updated regardless of the results of image tests in GitLab-CI.

Update master image
-------------------

In order to deploy a new version of the Jenkins master, create a new branch and
update the Dockerfile argument `JENKINS_VERS` to the appropriate value. Then
open a merge request. The new image will be built and deployed with tag `latest`.

The new image can now be tested. Note that existing instances use the `stable` tag by default
and will be not use the new image until the `stable` tag is updated.

In addition to GitLab-CI tests, verify the new image by editing a test instance's DeploymentConfig to point
to `jenkins-cern:latest` (also edit the reference in the `imageChangeParams` part).

To validate the correct initialization of a new Jenkins instance, `oc exec` in your test instance
and `rm -rf /var/lib/jenkins`, then delete the pod so a new pod is recreated and the instance is
re-initialized.

When satisfied:

* Tag the last commit in master with a meaningful git tag (usually with the jenkins version
  and an incremental release number e.g. `2.32.2-1`). This will import the new image into the production
  and development clusters, with a Docker tag equals to the git tag. It will also make available a manual
  trigger to mark the image as `stable` (and redeploy existing applications).

* publish an announcement on the SSB about the upcoming update and give
a couple hour time frame. (2-3 days before the actual deployment)

* During the announced time window,
run the manual triggers in the pipeline corresponding to the git tag to mark
that image as `stable`. This will trigger update and redeployment of all instances.
A [configurable grace period](https://jenkinsdocs.web.cern.ch/chapters/operations/config-graceful-shutdown.html)
is granted for Jenkins jobs to complete during redeployment.

Updating plugins:

When Jenkins runs for the first time or it detects a major OCP release, as it happened in September 2019 when OpenShift
released a 4.0 release, the plugins get re-installed.
This re-installation of the plugins can cause possible failures in the user's applications. If the users run higher
versions of the plugins they will be downgraded.
On the other hand, when re-installing them our experience says that there is a 50% chance of failure due to the presence
of older plugins (only when instances have custom plugins). We test that the default set of plugins survives the upgrade.

In this case `we do not decide if the plugins get re-installated in every upgrade`. For now this depends on OpenShift
platform version changes in the upstream image.
In case the plugins are reset, we need to mention it in the `OTG` for the users to be aware of possible failures.

To see if the plugins are going to be updated or not, we have to check the jobs in [jenkins-cern](https://gitlab.cern.ch/ci-tools/jenkins-cern/).
There are two different test jobs,**test_brand_new_instance** and **test_update_existing_instance**. When the plugins are updated, we can see
in the **test_update_existing_instance** job that `Installed plugins` is instantiated twice. This means that the plugins are going to be updated
in an already created instances.

When updating the plugins, it is also required to bump the version of the plugins we install manually. These plugins are set in `contrib/openshift/extra_plugins_no_overwrite.txt`.
We can find the plugin's version in `https://plugins.jenkins.io/<plugin_name>` (e.g. https://plugins.jenkins.io/gitlab-plugin).

### Template For Jenkins Upgrades - OTG

**Short description:** Upgrade of centrally managed Jenkins instances to LTS {version_number}
**Service element:** Continuous Integration with Jenkins
**Type:** Planned intervention
**Impact:** Degraded
**Description**
In order to fix a number of vulnerabilities recently announced in a security advisory [1], all centrally managed Jenkins instances will be upgraded to LTS
{version_number} version (See [2] for full changelog). During the time window of the intervention, all Jenkins instances will be restarted once.

Instance owners can configure the graceful shutdown of their instance to allow more time for jobs to finish before the restart. Check [3] for more info.

**Only add the following part if the plugins are going to be updated**
Centrally hosted Jenkins instances come with a number of plugins pre-installed, and the reference versions of these pre-installed plugins have also been updated
following multiple security issues in these plugins. This Jenkins upgrade will reset all pre-installed plugins to their updated reference version (which may not be
the very latest version of each plugin).

It is possible that these new plugin reference versions conflict with additional user-installed plugins, or that an instance requires a more recent version of a
pre-installed plugin than the new reference version. In this case, please visit Jenkins' plugin administration page after the upgrade to manually update any such plugin.

[1] {security_advisory_url}
[2] {changelog_of_last_version_url}
[3] [jenkins-docs](https://jenkinsdocs.web.cern.ch/chapters/operations/config-graceful-shutdown.html)

**Consequences:** User visible: yes, data loss: no, duration: 1 hour, permanent `solution:yes`

See for instance: [OTG0052620](https://cern.service-now.com/service-portal/view-outage.do?n=OTG0052620)

Reconfiguring existing instances
================================

### Applying changes to Jenkins configuration
After modifying the jenkins-cern image or the template, we might want to reconfigure existing instances.

See #23 for examples of what has been done in the past.

### Applying changes to the DeploymentConfig
After modifying the DeploymentConfig definition in the template, we might want to
apply the same changes to existing instances. Typically, we'll want to modify the
DeploymentConfig without triggering a redeploy - just to make sure the changes are
present for the next redeployment. Then we can redeploy manually or by tagging a new
image as `stable`.

See #42 for an example. The process
is as follows, adapt as necessary.

**NB: this has consistently triggered redeployment in prod cluster, while it did not
trigger redeployment in dev cluster. It's not understood why. Maybe we should not
do changes immediately after/before changing triggers and add temporization.**

```bash
instances=$(oc get dc --all-namespaces -l template=jenkins-cern  -o go-template='{{range .items}}{{.metadata.namespace}}{{println}}{{end}}')
for i in $instances; do
  # backup triggers and disable them, so we can do changes in the DC without triggering
  # a redeployment
  echo "Previous triggers for $i: $(oc get dc/jenkins -n $i -o go-template='{{.spec.triggers}}')";
  oldtriggers=$(oc get dc/jenkins -n $i -o json | jq .spec.triggers)
  oc set triggers dc/jenkins -n $i --manual
  # modify DC
  oc patch dc/jenkins -n $i -p '...'
  ...
  # restore triggers (This didn't cause a redeployment in my tests, but to be confirmed...)
  oc patch dc/jenkins -n $i -p '{"spec": {"triggers": '"${oldtriggers}"' }}'
done
```

Creating a Jenkins instance
=============================================

To create an app and test:

  * Create an Openshift project in [webservices](https://cern.ch/web)
    - NewSiteName: jenkins-_InstanceName_
    - SiteType: OpenShift
    - Category: Official
  * Go to Openshift console https://openshift.cern.ch and open the new project
  * Add to project -> select jenkins-cern template (click 'see all' if it is not visible in template list)
  * Set JENKINS_ADMIN_EGROUP to an e-group that will be granted admin access to the instance.
  * after a few minutes, one can connect to https://cern.ch/jenkins-_InstanceName_

Associating a build item with SLC6 or CC7 slaves
=========================================

  * By default, jenkins-slave-cc7 will be used (as this is the first image listed in the _cloud_ configuration)
  * Create a build item. Specify in "Restrict where this project can be run": jenkins-slave-cc7 or jenkins-slave-slc6

Running a static JNLP slave:
==========================

Running a static slave will consume more resources but a slave will be always available to process jobs, instead of having
to dynamically provision one. This allows much faster job startup as well as persisting some state between jobs.

Before doing this, it is important to check the quota usage of the project and see if it will make sense to increase
in order to fit the static slaves. With our current quota setting, a master with a single static slave will need at least
medium quota in order to allow Rolling updates

In Jenkins, register the node:

* name: test_static_slave
* Type: Java web start
* Remote root: /opt/app-root/jenkins
* Labels: jenkins-slave-cc7

Save and click the new slave node: this shows a command line like this

`java -jar slave.jar -jnlpUrl https://jenkins-ai-config-team.app.cern.ch/computer/test_static_slave/slave-agent.jnlp -secret f224413xxxxxxx`

Write down the secret string.

We assume the Jenkins service name is `jenkins`, this is the default. Slave pods can use that name to contact the Jenkins master.

Now we're ready to create the static slave from the standard CC7 slave image. An emptyDir is used for the Jenkins home directory:

```bash
oc create deploymentconfig jenkins-slave-cc7 --image=gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
oc set volumes dc/jenkins-slave-cc7 --add --type emptyDir --mount-path /home/jenkins
```

Then edit the DeploymentConfig generated by the above command with `oc edit dc/jenkins-slave-cc7`
to fill the `workDir`, `args` and `env` configuration for the container spec, following what the
[Kubernetes plugin does for dynamically provisioned pods](https://github.com/jenkinsci/kubernetes-plugin/blob/b6df05b6a8c91590214791ec74c6c06248a946c9/src/main/java/org/csanchez/jenkins/plugins/kubernetes/KubernetesCloud.java#L216).

```yaml
spec:
  template:
    spec:
      containers:
[...]
        args: ["f224413xxxxxxx", "test_static_slave" ]
        workingDir: /home/jenkins
        env:
        - name: JENKINS_URL
          value: http://jenkins:8080
        - name: JENKINS_LOCATION_URL
          value: https://jenkins-ai-config-team.web.cern.ch/
        - name: JENKINS_SECRET
          value: f224413xxxxxxx
        - name: JENKINS_JNLP_URL
          value: http://jenkins:8080/computer/test_static_slave/slave-agent.jnlp
        - name: JENKINS_HOME
          value: /home/jenkins
```

Automated testing
==========================

As part of the Continuous Integration jobs, both the newly built image and the template
are tested in two OpenShift projects, created specifically for this task in the [dev cluster](https://openshift-dev.cern.ch).
* [test-jenkins-new-deploy](https://openshift-dev.cern.ch/console/project/test-jenkins-cern-new-deploy): Create a new Jenkins instance
using the new built image and template and wait until it is up and ready.
* [test-jenkins-running](https://openshift-dev.cern.ch/console/project/test-jenkins-cern-running): First, create a new Jenkins instance
using the old template (stored in the `openshift` namespace) and the old Jenkins image. Once it is up and ready, re-deploy Jenkins instance
with the newly built image and check if it continues working.

These 2 projects are created as normal webservices applications, then we need to grant
admin access to the serviceaccount associated with the jenkins-cern resource/template:

```
oc adm policy add-role-to-user admin system:serviceaccount:openshift:jenkins-cern -n test-jenkins-cern-new-deploy
oc adm policy add-role-to-user admin system:serviceaccount:openshift:jenkins-cern -n test-jenkins-cern-running
```

As part of the testing we need to use the API, but https://jenkins.io/blog/2018/07/02/new-api-token-system/
makes it hard to automate. We need to inject a `config.xml` file with a well-known token for the admin user in instances and restart them.
A clear-text token needs to be generated randomly and set in this project's CI variables with name `JENKINS_API_TOKEN`.
We then need to generate the hash for that token and store it in [test/admin-config.xml](test/admin-config.xml).

Currently tokens start with a [fixed version number "11"](https://github.com/jenkinsci/jenkins/blob/master/core/src/main/java/jenkins/security/apitoken/ApiTokenStore.java#L68) then 32 hex digits that are hashed with SHA256 to generate the value stored in user config.xml.

So if we randomly generate a 32 hex digit value "28fc97585441498b00295660ec615620":
* the API token to use for authentication is "1128fc97585441498b00295660ec615620"
* the hashed value is sha256("28fc97585441498b00295660ec615620") = "f4e21256c2fa88ee2f5d43508454d19a3b3ca01c848edc19effdce009756b9c8"
* the user config.xml has the following content under `<properties>`: (actual values for `uuid` and `creationDate` don't matter as long as they are valid values)
```
    <jenkins.security.ApiTokenProperty>
      <tokenStore>
        <tokenList>
          <jenkins.security.apitoken.ApiTokenStore_-HashedToken>
            <uuid>df36e2fa-a339-4bab-821a-a9d0b3a32422</uuid>
            <name>test</name>
            <creationDate>2018-11-27 13:29:51.353 UTC</creationDate>
            <value>
              <version>11</version>
              <hash>f4e21256c2fa88ee2f5d43508454d19a3b3ca01c848edc19effdce009756b9c8</hash>
            </value>
          </jenkins.security.apitoken.ApiTokenStore_-HashedToken>
        </tokenList>
      </tokenStore>
    </jenkins.security.ApiTokenProperty>
```
