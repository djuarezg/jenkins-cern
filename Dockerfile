# CERN-customized Jenkins image for OpenShift

FROM quay.io/openshift/origin-jenkins:latest

# So we can overrride it with --build_arg
# See https://www.jenkins.io/changelog-stable/
ARG JENKINS_VERS='2.263.1'
ENV JENKINS_VERSION=$JENKINS_VERS

# this is merged with labels from upstream image
LABEL k8s.io.display-name="Jenkins ${JENKINS_VERSION}"

# switch to root user
USER 0

# install CERN centos8 repo so we can install the CERN CA
COPY ./contrib/yumrepos /etc/yum.repos.d/

# CERN-specific setup:
# 1. install CERN CA and other Centos/EPEL repos from CERN centos8 repo
# 2. configure Kerberos for CERN
# 3. make /etc/passwd writable so we can set a proper user in Openshift 3 (not needed in OKD4 with https://github.com/openshift/origin/issues/23369)
# 4. install xsltproc (package libxslt) and xmlstarlet, both needed by our run.cern startup script, from CERN centos 8 BaseOS/EPEL repo

RUN yum install -y --disablerepo="*" --enablerepo=cern CERN-CA-certs centos-linux-repos epel-release && \
    curl -s -L http://cern.ch/linux/docs/krb5.conf -o /etc/krb5.conf && \
    chmod 664 /etc/passwd && \
    yum clean all

RUN yum install -y libxslt xmlstarlet --disablerepo="*" --enablerepo=BaseOS --enablerepo=epel

# Overwrite jenkins repo with the LTS repository, update Jenkins to the specific JENKINS_VERSION
RUN curl -s -L http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo -o /etc/yum.repos.d/jenkins.repo && \
    rpm --import http://pkg.jenkins-ci.org/redhat/jenkins.io.key && \
    yum install -y --disablerepo="*" --enablerepo=jenkins jenkins-${JENKINS_VERSION} && \
    yum clean all

# deploy custom run script (run.cern)
COPY ./contrib/s2i /usr/libexec/s2i

# customize configuration files
COPY ./contrib/openshift /opt/openshift

# Install well-known SSH host keys for GitLab
COPY ./contrib/ssh /etc/ssh

# install extra plugins and ensure permissions are correct everywhere after adding our own files
# Plugins go to /opt/openshift/plugins in the image, then on first run they'll be copied to /var/lib/jenkins
RUN /usr/local/bin/install-plugins.sh /opt/openshift/extra_plugins_no_overwrite.txt && \
    /usr/local/bin/install-plugins.sh /opt/openshift/extra_plugins_overwriten_at_startup.txt && \
    chown -R 1001:0 /opt/openshift && \
    /usr/local/bin/fix-permissions /opt/openshift && \
    /usr/local/bin/fix-permissions /var/lib/jenkins

# switch back to unpriviledged user
USER 1001

# We will need this once we change the base image to latest.
# run our custom startup script instead of upstream one
# upstream ENTRYPOINT was ["/usr/bin/go-init", "-main", "/usr/libexec/s2i/run"]
# in https://github.com/openshift/jenkins/blob/master/2/Dockerfile.localdev
ENTRYPOINT ["/usr/bin/go-init", "-main", "/usr/libexec/s2i/run.cern"]
