#!/bin/bash
# Jenkins runs into issues when several instances are running concurrently. If we start this instance
# while another one is still gracefully terminating (hence still processing jobs), the full job history
# will not be visible in this instance.
# Openshift/Kubernetes does not take this into account when restarting pods.
# So do not actually start Jenkins until other instances have exited completely.

# TODO: discover deploymentconfig label value on self using downward API volume
# https://docs.openshift.com/enterprise/3.0/dev_guide/downward_api.html#dapi-using-volume-plugin
# (we can also use that to filter out the current pod's name from the pod list and show
# what are the other Running pods)
INSTANCE_MUTEX_DEPLOYMENTCONFIG=${INSTANCE_MUTEX_DEPLOYMENTCONFIG:-jenkins}
INSTANCE_MUTEX_MAX_SLEEP_INTERVAL=${INSTANCE_MUTEX_MAX_SLEEP_INTERVAL:-120}

LIST_TERMINATING_PODS_TEMPLATE='{{range .items}}{{if and (eq .status.phase  "Running") (.metadata.deletionTimestamp)}}{{.metadata.name}}{{println}}{{end}}{{end}}'

function count_terminating_pods {
  oc get pods -l deploymentconfig=${INSTANCE_MUTEX_DEPLOYMENTCONFIG} -o go-template --template "${LIST_TERMINATING_PODS_TEMPLATE}" | wc -l
}
# check if any other pod is still running with label deploymentconfig=jenkins
sleep_interval=1

while [ $(count_terminating_pods) -gt 0 ]; do
  echo "Other containers are still gracefully terminating for this Jenkins instance. Waiting ${sleep_interval}s before starting..."
  sleep $sleep_interval
  sleep_interval=$[ $sleep_interval * 2]
  if [ $sleep_interval -gt $INSTANCE_MUTEX_MAX_SLEEP_INTERVAL ];then sleep_interval=$INSTANCE_MUTEX_MAX_SLEEP_INTERVAL; fi
done
